#ifndef DES_DES_H
#define DES_DES_H

#include "des_types.h"

bit64 des_encrypt(bit64 pt_data, bit64* round_keys);
bit64 des_decrypt(bit64 ct_data, bit64* round_keys);


#endif //DES_DES_H
