#ifndef DES_DES_CBC_H
#define DES_DES_CBC_H

#include "des_types.h"

#define FULL_PKCS7_PADDING 0x808080808080808
#define FULL_1BIT_PADDING 0x8000000000000000

#define ONEBITPADDING 0 // 1 for 1bit padding, 0 for PKCS7 padding

bit64* des_cbc_encrypt(const bit64* data, size_t size, bit64 key);
bit64* des_cbc_decrypt(const bit64* ct_data, size_t size, bit64 key, bit64 iv);

#endif //DES_DES_CBC_H
