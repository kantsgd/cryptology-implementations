#ifndef DES_DES_KS_H
#define DES_DES_KS_H

#include "des_types.h"

// x: number to rotate, n: how many times to rotate, s: sizeof x
#define ROTL(x, n, s) (((x << n % 28) | (x >> (28-n) % 28)) & 0xFFFFFFF)
// outputs c concatenate d in bit level
#define APPEND(c, d) ((c << 28) | d)
#define ROUND_KEYS_ARR_SIZE 16

bit64 pc1_c(bit64 k);
bit64 pc1_d(bit64 k);
bit64 pc_2(bit64 cd);
bit64* key_scheduler(bit64 key);

#endif //DES_DES_KS_H
