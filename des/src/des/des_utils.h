#ifndef DES_DES_UTILS_H
#define DES_DES_UTILS_H

#include "des_types.h"

static short last_block_size(bit64 val) {
    short size = 1;
    for (short i = 0; i < 8; i++) {
        val >>= 8;
        if (val == 0) {
            break;
        }
        size++;
    }
    return size;
}

#endif //DES_DES_UTILS_H
