#include <stdlib.h>
#include "des_cbc.h"
#include "des_ks.h"
#include "des.h"
#include "des_utils.h"

#ifdef __unix__
#include <unistd.h>
#elif defined(_WIN32)
#include <windows.h>
#include <wincrypt.h>
#endif

bit64 apply_pkcs7_padding(bit64 last_pt_block, short size_of_last_block) {
    bit64 last_block = 0;
    if (size_of_last_block < 8) {
        last_block = last_pt_block;
        bit64 shifted_data = last_block << (64 - (size_of_last_block * 8));
        for (int i = 0; i < (8 - size_of_last_block); i++) {
            bit64 padding = ((bit64)(8 - size_of_last_block)) << (i * 8);
            shifted_data |= padding;
            last_block = shifted_data;
        }
    } else if (size_of_last_block == 8) {
        last_block = FULL_PKCS7_PADDING;
    } else {
        // invalid block size
    }
    return last_block;
}

bit64 apply_1bit_padding(bit64 last_pt_block, short size_of_last_block) {
    bit64 last_block = 0;
    if (size_of_last_block < 8) {
        last_block = last_pt_block;
        bit64 shifted_data = last_block << (64 - (size_of_last_block * 8));
        bit64 padding = ((bit64)(0x1)) << (64 - (size_of_last_block * 8) - 1);
        last_block = shifted_data | padding;
    } else if (size_of_last_block == 8) {
        last_block = FULL_1BIT_PADDING;
    } else {
        // invalid block size
    }
    return last_block;
}

bit64 generate_iv() {
    bit64 concataneted_iv;
#ifdef __unix__
    unsigned char iv[8];
    getentropy(iv, 8);
    concataneted_iv = ((bit64)iv[0]) << 56 | ((bit64)iv[1]) << 48 | ((bit64)iv[2]) << 40 | ((bit64)iv[3]) << 32 |
        ((bit64)iv[4]) << 24 | ((bit64)iv[5]) << 16 | ((bit64)iv[6]) << 8 | ((bit64)iv[7]);
#elif defined(_WIN32)
    HCRYPTPROV   hCryptProv;
    BYTE         pbData[8];
    if (CryptAcquireContext(&hCryptProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) == FALSE) {
        exit(1); // Can not get random iv, exit
    }
    if (CryptGenRandom(hCryptProv, 8, pbData)) {
        concataneted_iv = ((bit64)pbData[0]) << 56 | ((bit64)pbData[1]) << 48 | ((bit64)pbData[2]) << 40 | 
            ((bit64)pbData[3]) << 32 | ((bit64)pbData[4]) << 24 | ((bit64)pbData[5]) << 16 | ((bit64)pbData[6]) << 8 |
            ((bit64)pbData[7]);
    }
#endif
    return concataneted_iv;
}

// pt_data: plaintext data in array, size: sizeof pt_data, key: secret key
// function returns cipher_data in array. last index always contains iv.
// If necessary, applies padding.
bit64* des_cbc_encrypt(const bit64* pt_data, size_t size, bit64 key) {
    if (pt_data == NULL) {
        return NULL;
    }
    const short size_of_last_block = last_block_size(pt_data[size - 1]);
    bit64* cipher_data;
    // generate iv
    bit64 iv = generate_iv();

    // apply padding
#ifdef ONEBITPADDING
    // apply 1 bit padding
    bit64 last_block = apply_1bit_padding(pt_data[size-1], size_of_last_block);
#else
    // apply pkcs7 padding
    bit64 last_block = apply_pkcs7_padding(pt_data[size-1], size_of_last_block);
#endif

    // des encrypt in cbc mode
    size_t cipher_size = size * sizeof(bit64) + sizeof(bit64);
    if (size_of_last_block == 8) {
        cipher_size += sizeof(bit64); // padding bytes
    }
    cipher_data = malloc(cipher_size); // last bit64 is for iv.
    bit64 xor_data = iv;
    bit64* round_keys = key_scheduler(key);
    long i = 0;
    for (; i < size-1; i++) {
        bit64 cipher = des_encrypt(pt_data[i] ^ xor_data, round_keys);
        cipher_data[i] = cipher;
        xor_data = cipher;
    }
    // encrypt last block
    if (size_of_last_block < 8) {
        // encrypt only last block
        bit64 cipher = des_encrypt(last_block ^ xor_data, round_keys);
        cipher_data[i++] = cipher;
    } else {
        // encrypt last block + padding block
        bit64 cipher = des_encrypt(pt_data[i] ^ xor_data, round_keys);
        cipher_data[i++] = cipher;
        xor_data = cipher;

        cipher = des_encrypt(last_block ^ xor_data, round_keys);
        cipher_data[i++] = cipher;
    }
    cipher_data[i] = iv;
    return cipher_data;
}

// ct_data: ciphertext, size: size of ciphertext, key: secret key, iv: initialization vector
bit64* des_cbc_decrypt(const bit64* ct_data, size_t size, bit64 key, bit64 iv) {
    if (ct_data == NULL) {
        return NULL;
    }
    bit64* plaintext_data;
    bit64* round_keys = key_scheduler(key);

#ifdef ONEBITPADDING
    // check 1bit padding
    bit64 last_block = ct_data[size - 1];
    if (last_block == FULL_1BIT_PADDING) {
        plaintext_data = malloc((size-1) * sizeof(bit64));
    } else {
        plaintext_data = malloc(size * sizeof(bit64));
    }
#else
    // check PKCS#7 padding
    bit64 last_block = ct_data[size-1];
    if (last_block == FULL_PKCS7_PADDING) {
        plaintext_data = malloc((size-1) * sizeof(bit64));
    } else {
        plaintext_data = malloc(size * sizeof(bit64));
    }
#endif
    // decrypt ciphertext
    bit64 xor_data = iv;
    size_t i = 0;
    for (; i < size-1; i++) {
        bit64 decrypted_block = des_decrypt(ct_data[i], round_keys);
        plaintext_data[i] = decrypted_block ^ xor_data;
        xor_data = ct_data[i];
    }
    // remove 1bit padding from plaintext
#ifdef ONEBITPADDING
    if (last_block == FULL_1BIT_PADDING) {
        return plaintext_data;
    }
    else {
        // decrypt last block
        bit64 decrypted_block = des_decrypt(ct_data[i], round_keys);
        bit64 last_decrypted_block =  decrypted_block ^ xor_data;
        // remove 1bit padding
        for (int j = 1; j <= 64; j++) {
            bit64 temp = last_decrypted_block >> j;
            if ((temp & 0x1) == 1) {
                plaintext_data[i] = (last_decrypted_block >> (j+1));
                break;
            }
        }
    }
    // remove pkcs7 padding from plaintext
#else
    if (last_block == FULL_PKCS7_PADDING) {
        return plaintext_data;
    }
    else {
        // decrypt last block
        bit64 decrypted_block = des_decrypt(ct_data[i], round_keys);
        bit64 last_decrypted_block =  decrypted_block ^ xor_data;
        // remove padding
        size_t padding_size = last_decrypted_block & 0xFF;
        last_decrypted_block >>= (8 * padding_size);
        plaintext_data[i] = last_decrypted_block;
    }
#endif
    return plaintext_data;
}