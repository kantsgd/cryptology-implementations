#ifndef DES_DES_TYPES_H
#define DES_DES_TYPES_H

typedef unsigned long long int bit64;
typedef unsigned int bit32;
typedef unsigned char bit8;

#endif //DES_DES_TYPES_H
