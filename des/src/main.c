#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "des/des.h"
#include "des/des_cbc.h"
#include "des/des_ks.h"
#include "des/des_utils.h"


void print_initial_data(bit64* initial, size_t size) {
    printf("Initial Data: ");
    for(size_t i = 0; i < size; i++) {
        printf("%llx", initial[i]);
    }
    printf("\n");
}

void print_plaintext(bit64* plaintext, size_t size) {
    printf("Plaintext: ");
    for(size_t i = 0; i < size; i++) {
        printf("%llx", plaintext[i]);
    }
}

void print_ciphertext(bit64* ciphertext, size_t size) {
    printf("Ciphertext: ");
    for(size_t i = 0; i < size-1; i++) {
        printf("%llx", ciphertext[i]);
    }
}

void q1q2() {
    printf("Q1 & Q2\n");
    const bit64 SECRET_KEY = 0x436968616e676972;
    bit64 data[] = {0x5361697420476F6B, 0x74756720446F6761, 0x6E}; // Sait Goktug Dogan

    const size_t DATA_SIZE = sizeof(data) / sizeof(bit64);
    const short LAST_BLOCK_SIZE = last_block_size(data[DATA_SIZE - 1]);
    // initial data
    print_initial_data(data, DATA_SIZE);
    // ciphertext
    bit64* ciphertext = des_cbc_encrypt(data, DATA_SIZE, SECRET_KEY);
    size_t cipher_size = DATA_SIZE + (LAST_BLOCK_SIZE == 8 ? 2 : 1); // If last block size is 8, last cipher block will be padding block
    print_ciphertext(ciphertext, cipher_size);
    // iv
    bit64 iv = ciphertext[cipher_size - 1];
    printf("\nIV: %llx\n", iv);
    // plaintext
    bit64* plaintext = des_cbc_decrypt(ciphertext, cipher_size-1, SECRET_KEY, iv);
    print_plaintext(plaintext, DATA_SIZE);

    free(ciphertext); free(plaintext);
}

void q3() {
    printf("\n\nQ3\n");
    bit64* round_keys = key_scheduler(0x0123456789abcdef);
    printf("64MB DES started\n");
    clock_t begin = clock();
    for (int i = 0; i < 8388608; i++) {
        des_encrypt(0x4e6f772069732074, round_keys);
    }
    clock_t end = clock();
    printf("64MB DES ended, total execution time is: %f seconds\n", (double)(end - begin) / CLOCKS_PER_SEC);
}

void main() {
    q1q2();
    q3();
}