# Data Encryption Standard (DES)
>> IMPORTANT: This algorithm should never be used since it is easily crackable ! Use AES instead.

## General Information about DES

***DES*** is a block cipher algorithm used for data encryption. Although it is named as standard, it is not a standard.
This algorithm is also called **DEA** (Data Encryption Algorithm).

**For data encryption, AES should be used instead of DES. If DES must be used for any reason, use TDEA (Triple Data
Encryption Algorithm).**

Because DES uses 64 bit key, it is weak against brute force attacks and can be easily crackable.

DES documentation: [Doc](https://csrc.nist.gov/pubs/fips/46-3/final)

## DES Properties
* Block size: 64 bit
* Key size: 64 bit (Effective 56 bit)
* Rounds: 16
* Type: Feistel

## About This Project
This project provides an implementation for basic DES & DES with CBC (Cipher Block Chaining) mode in ***C*** language.
In CBC mode, if necessary, the implementation applies **[PKCS#7](https://en.wikipedia.org/wiki/PKCS_7)** padding to a plaintext before encryption.

>> NOTE: I did not test this implementation in **Windows** and **MacOS**. It may not work. For now, it is proven to work in **Linux**.

## Example Usage

``` c
// DATA PREPERATION
#include "des/des.h"     // DES implementation
#include "des/des_cbc.h" // DES with CBC mode implementation
#include "des/des_ks.h"  // DES key scheduler

const bit64 SECRET_KEY = 0x436968616e676972; // Your 64-bit secret key to be used in encryption.
bit64 plaintext[] = {0x5361697420476F6B, 0x74756720446F6761, 0x6E}; // Your plaintext to be encrypted, seperated with 64-bit blocks.
```

``` c
// Encryption with DES.
// Generate round keys to be used by DES.
bit64* round_keys = key_scheduler(key);
// Encrypt plaintext, produces 64-bit output ciphertext.
bit64 ciphertext = des_encrypt(plaintext, round_keys);
```

``` c
// Decryption with DES.
// Generate round keys to be used by DES.
bit64* round_keys = key_scheduler(key);
// Decrypt ciphertext, produces 64-bit output plaintext.
bit64 plaintext = des_decrypt(plaintext, round_keys);
```

``` c
// Encryption with DES-CBC.
// PT_DATA_SIZE: size of plaintext array
bit64* ciphertext = des_cbc_encrypt(plaintext, PT_DATA_SIZE, SECRET_KEY);
// Last item of ciphertext array is IV (Initialization Vector).
```

``` c
// Decryption with DES-CBC.
// CT_DATA_SIZE: size of ciphertext array.
bit64* plaintext = des_cbc_decrypt(ciphertext, CT_DATA_SIZE, SECRET_KEY, iv);
```